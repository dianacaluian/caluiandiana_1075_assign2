
const FIRST_NAME = "Diana";
const LAST_NAME = "Caluian";
const GRUPA = "1075";

/**
 * Make the implementation here
 */

   function initCaching() {
    var cache={
        about:0,
        home:0,
        contact:0,
        pageAccessCounter: function(value) {
            if(typeof value !== 'undefined'){
                if(value.toLowerCase()=='about') {
                    this.about++;
                }
                if(value.toLowerCase()=='contact'){
                    this.contact++;
                }
            }   
         else {
            this.home++;
         }
        },
        getCache: function(){
            return this;
        }
    };
   return cache; 
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

